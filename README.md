Ideias Compartilhadas
========================
## Um punhado de scripts, documentos e ideias compartilhadas com todos.

Ao longo do meu tralhalho consegui desenvolver alguns scripts que podem ajudar à todos.

### Como utilizar?

Scripts são simplesmente alguns comandos em shell que devem ser executados numa sequência lógica para surtirem algum efeito.

Então basta baixa-los em seu Linux preferido e torná-lo executável através do chmod. Exemplo:

`chmod +x nome-do-script.sh`

Depois é só executar:

`./nome-do-script.sh`

### Porquê estou disponibilizando tais conteúdos?

Apenas para que possa ser útil à alguém.



